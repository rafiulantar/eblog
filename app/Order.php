<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=['name','phone','email','user_id','shipping_address','product_id'];

        public function user(){

        return $this->belongsTo(User::class);
    }
}
