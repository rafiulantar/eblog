<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
//    protected $table = 'categories';

    protected $fillable = ['title', 'description', 'is_active'];

//      public $timestamps = false;

    /*
     * Relationship
     * */
    public function products() //1 to n
    {
        return $this->hasMany(Product::class);
    }

}
