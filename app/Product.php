<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['category_id', 'title', 'description', 'picture', 'is_active', 'price'];

    /*
     * Relationship
     * */
    public function category()// inverse
    {
        return $this->belongsTo(Category::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class);
    }

    //1 to n polymorphic relation
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function orders(){

        return $this->belongsTo(Order::class);

    }

}
