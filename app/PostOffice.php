<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostOffice extends Model
{
    protected $fillable=['name'];
    public function orders()
    {
        return $this->hasManyThrough(Order::class,User::class);
    }
}
