<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exports\CategoryExport;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Excel;
use PDF;


class CategoryController extends Controller
{

    //

    public function index()

    {





        $categories=Category::orderBy('created_at','desc')->get();
        //$categories=Category::onlyTrashed()->orderBy('created_at','desc')->get();
        return view('backend.categories.index',compact('categories'));
    }

    public function create()
    {
        return view('backend.categories.create');
    }
    public function store(Request $request)
    {

        try{

                $validatedData = $request->validate([
                    'title' => 'required|unique:categories|max:10',
                    'description' => 'min:5|max:1000|required',
                      ]);


                    $requestData=$request->all();
                    $requestData['is_active']=$request->is_active =='on'?1:0;

                    Category::create($requestData);
                   // $request->session()->flash('status', 'Task was successful!');
                    return redirect()->route('categories.index');

        }catch(QueryException $exception){

            return redirect()->back()->withErrors($exception->getMessage());
        }

       // $requestData=[
       //     'title'=>$request->title,
       //     'description'=>$request->description,
        //    'is_active'=>$request->is_active =='on'?1:0
        //];

        //$requestData=$request->all();
        //$requestData['is_active']=$request->is_active =='on'?1:0;

        //$category=Category::create($requestData);
        //dd($category);
    }
    public function show(Category $category)

    {

        //$category= Category::findOrFail($id);
        return view('backend.categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
       // $category= Category::findOrFail($id);

        return view('backend.categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {

        try{

//            $category = Category::findOrFail($id);
//        $requestData = [
//                            'title'=>$request->title,
//                            'description'=>$request->description,
//                            'is_active'=>$request->is_active == 'on'?1:0
//                        ];

            $requestData = $request->all();
            $requestData['is_active'] = $request->is_active == 'on'?1:0;

            $category->update($requestData);

//            $request->session()->flash('status', 'Task was successful!');

            return redirect()->route('categories.index')->withStatus('Task was successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }

     public function destroy(Category $category)
     {
     // dd('Updating');

     try{

           // $category=Category::findOrfail($id);
            $category->delete();
            return redirect()->route('categories.index');

     }catch(QueryException $exception){

            return redirect()->back()->withErrors($exception->getMessage());
     }
    }

    public function downloadPdf()
    {
        $categories= Category::all();
        //dd($categories);
        $pdf = PDF::loadView('backend.categories.pdf', compact('categories'));
        return $pdf->download('invoice.pdf');
    }
    public function downloadExcel()
    {
        //$categories= Category::all();
         return Excel::download(new CategoryExport, 'categories.xlsx');

    }
   public function trash()
    {
        $categories = Category::onlyTrashed()->orderBy('created_at', 'desc')->get();
        return view('backend.categories.trash', compact('categories'));
    }

    public function restore($id)
    {
        Category::withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect()->route('categories.trash')->withStatus('Restore was successful!');
    }

    public function forceDelete($id)
    {
      try {
        Category::withTrashed()
            ->where('id', $id)
            ->forceDelete();
          return redirect()->route('categories.trash')->withStatus('Delete was successful!');

                    }catch (QueryException $exception){
                return redirect()->back()->withErrors($exception->getMessage());
                }
    }






}
