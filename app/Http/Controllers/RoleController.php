<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('backend.roles.index', compact('roles'));
    }


    public function edit(Role $role)
    {
        $permissions = Permission::pluck('name', 'id')->toArray();
        $selectedPermissions = $role->permissions->pluck('id')->toArray();

        return view('backend.roles.edit', compact('role', 'permissions', 'selectedPermissions'));
    }
    public function store(Request $request)
    {
        $requestData=$request->all();
        Role::create($requestData);

        return redirect()->route('roles.index');
    }

    public function update(Request $request, Role $role)
    {

        $role->update(['name'=>$request->name]);

        $role->permissions()->sync($request->permissions);

        return redirect()->route('roles.index')->withStatus('Updated');

    }
    public function show(Role $role)
    {
        return view('backend.roles.show',compact('role'));
    }
    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->route('roles.index');
    }
    public function create(Role $role)
    {
        $permissions = Permission::pluck('name', 'id')->toArray();
        $selectedPermissions=$role->permissions->pluck('id')->toArray();

        return view('backend.roles.create',compact('permissions','selectedPermissions'));
    }

}
