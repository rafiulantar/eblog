<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        return view('backend.users.index', compact('users'));
    }

    public function edit(User $user)
    {

        $roles = Role::pluck('name', 'id')->toArray();
        $selectedRoles = $user->roles->pluck('id')->toArray();

        return view('backend.users.edit', compact('user', 'roles', 'selectedRoles'));
    }

    public function update(Request $request, User $user)
    {

        $data = $request->all();

        $userData = [
            'name' => $data['name'],
            'age' => $data['age'],
            'email' => $data['email'],
            'active_role_id' => $data['active_role_id'],
        ];

        if($request->password){
            $userData['password'] = Hash::make($data['password']);
        }

        $user->update($userData);

        $user->profile()->update([
            'bio' => $data['bio'],
            'facebook_uri' => $data['facebook_uri'],
        ]);

        $user->roles()->sync($request->roles);

        return redirect()->route('users.index')->withStatus('Updated');

    }

    public function profile()
    {
        $profile = auth()->user()->profile;
        dd($profile);

    }
    public function show(User $user)
    {
        return view('backend.users.show', compact('user'));
    }
}
