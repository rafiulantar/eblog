<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Cart;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function shoppingCart()
    {
        $productIds = Cookie::get('productIds');
        $products = [];
        if(!is_null($productIds)){
            $productIds = unserialize($productIds);
            $products = Product::whereIn('id', $productIds)->get();
        }

        $productsInCart = $productIds;

        return view('frontend.cart', compact('products', 'productsInCart'));
    }


    public function removeFromCart($productId)
    {

        $productIds = Cookie::get('productIds');
        if(!is_null($productIds)){
            $productIds = unserialize($productIds);
            $key = array_search($productId, $productIds);
            unset($productIds[$key]);
            $data = serialize($productIds);
        }
        Cookie::queue(Cookie::forget('productIds'));
        Cookie::queue('productIds', $data , 3600*10);
        return redirect()->route('cart');

    }

    public function addToCart(Request $request)
    {

        $productIds = Cookie::get('productIds');

        if(!is_null($productIds)){
            $productIds = unserialize($productIds);
        }

        if(is_array($productIds) && (count($productIds)>0)){
            if(!in_array($request->product_id, $productIds)){
                array_push($productIds, $request->product_id);
            }
            $data = serialize($productIds);
        }else{
            $data = serialize([$request->product_id]);
        }

        Cookie::queue('productIds', $data , 3600*10);

        return response()->json($productIds);

    }

    public function index()
    {
        return view('frontend.cart');
    }
    public function store(Request $request)
    {

        $this->validate($request,[
            'product_id' => 'required'
        ],
        [
            'product_id.required'=> 'Please give a product'
        ]);
        if (Auth::check()) {
            $cart = Cart::where('user_id', Auth::id())
            ->where('product_id', $request->product_id)
            ->first();
        }else{

        }



            return back();

    }

    public function update(Request $request, $id)
    {
        $cart = Cart::find($id);
        if (!is_null($cart)){
            $cart->product_quantity = $request->product_quantity;
            $cart->save();
        }else{
            return redirect()->route('cart');
        }
        return back()->withStatus('Update was successful!');
    }

    public function destroy($id)
    {
        $cart = Cart::find($id);
        if (!is_null($cart)){
            $cart->delete();
        }else{
            return redirect()->route('cart');
        }
        return back()->withStatus('Delete was successful!');
    }


}
