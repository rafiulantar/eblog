<?php

namespace App\Http\Controllers;

use App\Size;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SizeController extends Controller
{

    public function index()
    {
        $sizes = Size::orderBy('created_at', 'desc')->get();
        return view('backend.sizes.index', compact('sizes'));
    }

    public function create()
    {
        return view('backend.sizes.create');
    }

    public function store(Request $request)
    {
//        try{

        $request->validate([
            'title' => 'required|unique:sizes|max:50',
        ]);

//        $requestData = [
//                            'title'=>$request->name,
//                            'description'=>$request->description,
//                            'is_active'=>$request->is_active == 'on'?1:0
//                        ];

        $requestData = $request->all();
        Size::create($requestData);

//            $request->session()->flash('status', 'Task was successful!');

        return redirect()->route('sizes.index')->withStatus('Task was successful!');

//        }catch (QueryException $exception){
//            return redirect()->back()->withInput()->withErrors($exception->getMessage());
//        }

    }

    public function show(Size $size) //dependency injection
//    public function show($id) //dependency injection
    {


//        $size = Size::findOrFail($id);
//        dd($size);
        return view('backend.sizes.show', compact('size'));
    }

    public function edit(Size $size)
    {
//        $size = Size::findOrFail($id);

        return view('backend.sizes.edit', compact('size'));
    }

    public function update(Request $request, Size $size)
    {

        try{

            $requestData = $request->all();
            $size->update($requestData);

//            $request->session()->flash('status', 'Task was successful!');

            return redirect()->route('sizes.index')->withStatus('Task was successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }

    public function destroy(Size $size)
    {

        try{

//            $size = Size::findOrFail($id);
            $size->delete();

            return redirect()->route('sizes.index')->withStatus('Task was successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }
}
