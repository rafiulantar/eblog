<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications=auth()->user()->notifications;

        foreach($notifications as $notification){
            $notification->markAsRead();
        }

    }
}
