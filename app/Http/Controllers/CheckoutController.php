<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    public function index()
    {
        return view('frontend.checkout');
    }

    public function store(Request $request)
    {
        $order= new Order();
        $order->name=$request->name;
        $order->email=$request->email;
        $order->phone=$request->phone;
        $order->shipping_address=$request->shipping_address;
        $order->user_id=Auth::id();
        $order->save();

        return redirect()->back();


    }
}
