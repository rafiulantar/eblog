<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Size;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class ProductController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->get();
//        $products = Product::onlyTrashed()->orderBy('created_at', 'desc')->get();
        return view('backend.products.index', compact('products'));
    }

    public function create()
    {

//        $categories = [
//            '1'=>'Category 1',
//            '2'=>'Category 2',
//        ];

        $categories = Category::pluck('title', 'id')->toArray();
        $sizes = Size::pluck('title', 'id')->toArray();
        $selectedSizes = [];

        return view('backend.products.create', compact('categories', 'sizes', 'selectedSizes'));
    }

    public function store(Request $request)
    {
        try{
            $request->validate([
                'title' => 'required|unique:products|max:100',
                'category_id' => 'required|exists:categories,id',
                'description' => 'min:1|max:1000|required',
            ]);

            $requestData = $request->all();

            if ($request->hasFile('picture')) {
                $requestData['picture'] = $this->uploadImage($request->picture, $request->title);
            }

            $requestData['is_active'] = $request->is_active == 'on'?1:0;

            $category = Category::findOrFail($request->category_id);

            $product = $category->products()->create($requestData);

            $product->sizes()->attach($request->size);

            return redirect()->route('products.index')->withStatus('Task was successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

    }

    public function show(Product $product) //dependency injection
//    public function show($id) //dependency injection
    {

//        $product = Product::findOrFail($id);
//        dd($product);
        return view('backend.products.show', compact('product'));
    }

    public function edit(Product $product)
    {
//        $product = Product::findOrFail($id);

        $categories = Category::pluck('title', 'id')->toArray();

        $sizes = Size::pluck('title', 'id')->toArray();

        $selectedSizes = $product->sizes->pluck('id')->toArray();

        return view('backend.products.edit', compact('product', 'categories', 'sizes', 'selectedSizes'));
    }

    public function update(Request $request, Product $product)
    {

        try{

            $requestData = $request->all();

            if ($request->hasFile('picture')) {

                $this->unlink($product->picture);

                $requestData['picture'] = $this->uploadImage($request->picture, $request->title);
            }else{
                $requestData['picture'] = $product->picture;
            }

            $requestData['is_active'] = $request->is_active == 'on'?1:0;

            $product->update($requestData);

            $product->sizes()->sync($request->size);

//            $request->session()->flash('status', 'Task was successful!');

            return redirect()->route('products.index')->withStatus('Task was successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }

    public function destroy(Product $product)
    {

        try{

//            $product = Product::findOrFail($id);
            $product->sizes()->detach();
            $product->delete();

            return redirect()->route('products.index')->withStatus('Task was successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }

    private function uploadImage($file, $name)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

        $file_name = $timestamp .'-'.$name. '.' . $file->getClientOriginalExtension();

        $pathToUpload = storage_path().'/app/public/products/';

        Image::make($file)->resize(700, 400)->save($pathToUpload.$file_name);

        return $file_name;
    }

    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/products/';

        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }

}
