<?php

namespace App\Http\Middleware;

use Closure;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=auth()->user();
        if($user->age>=100){

            session()->flash('status','You are not acess to admin panel');
            return redirect('/');
        }else{
            return $next($request);
        }
    }
}
