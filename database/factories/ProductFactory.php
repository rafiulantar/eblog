<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Product;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {

    $placeholder=['abstract','nature','sports','technics','transport'];
    $img_type= $faker->randomElement($placeholder);


    return [


        'title' => $faker->name,
        'picture' =>$faker->unique()->image(public_path('uploads/products'),$width = 640, $height = 480, $img_type,false),
        'description' => $faker->realText(15),
        'is_active'=>$faker->randomElement([true,false])
    ];
});
