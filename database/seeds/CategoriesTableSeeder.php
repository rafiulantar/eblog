<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // \App\Category::create(['title'=>'Women Clothing', 'description'=>'Summer Collection']);
        $data=[
        ['title'=>'Men Clothing','description'=>'Summer Collection','is_active'=>false],
        ['title'=>'Child Clothing','description'=>'Summer Collection','is_active'=>True],
        ['title'=>'Women Clothing','description'=>'Summer Collection','is_active'=>True]
        ];

        foreach($data as $value){
        App\Category::create($value);
        }
    }
}
