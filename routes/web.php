<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//get, post, put, patch, delete http verb

Auth::routes();



Route::middleware(['auth', 'ageChecker'])->group(function (){

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/profile', 'UserController@profile')->name('users.profile');

    Route::post('/products/{product}/review', 'PublicController@review')->name('products.review');

    /*Frontend*/
    Route::get('/products', 'PublicController@products');
    Route::get('/product_details', 'PublicController@show');

    Route::prefix('admin')->group(function (){
        /*Category routes*/
        Route::get('/categories/download-pdf', 'CategoryController@downloadPdf')->name('categories.download_pdf');
        Route::get('/categories/download-excel', 'CategoryController@downloadExcel')->name('categories.download_excel');
        Route::get('/categories/trash', 'CategoryController@trash')->name('categories.trash');
        Route::resource('/categories', 'CategoryController');
        Route::resource('/products', 'ProductController');
        Route::resource('/sizes', 'SizeController');
        Route::resource('/roles', 'RoleController');


        Route::resource('/orders', 'OrderController');
        Route::resource('/users', 'UserController');


        Route::get('/users', 'UserController@index')->name('users.index');
        Route::get('/users/{user}', 'UserController@show')->name('users.show');
        Route::get('/users/{user}/edit', 'UserController@edit')->name('users.edit');
        Route::put('/users/{user}', 'UserController@update')->name('users.update');
        Route::delete('/users/{user}', 'UserController@destroy')->name('users.destroy');





        Route::get('/roles', 'RoleController@index')->name('roles.index');
        Route::get('/roles/{role}', 'RoleController@show')->name('roles.show');
        Route::get('/roles/{role}/edit', 'RoleController@edit')->name('roles.edit');
        Route::put('/roles/{role}', 'RoleController@update')->name('roles.update');
        Route::delete('/roles/{role}', 'RoleController@destroy')->name('roles.destroy');


        Route::delete('/system/notifications', 'NotificationController@index')->name('notifications.all');


//    Route::get('/categories', 'CategoryController@index')->name('categories.index');//list
//    Route::get('/categories/create', 'CategoryController@create')->name('categories.create');//create
//    Route::post('/categories', 'CategoryController@store')->name('categories.store');//store
//    Route::get('/categories/{category}', 'CategoryController@show')->name('categories.show');//show
//    Route::get('/categories/{category}/edit', 'CategoryController@edit')->name('categories.edit');//edit
//    Route::put('/categories/{category}', 'CategoryController@update')->name('categories.update');//update
//    Route::delete('/categories/{category}', 'CategoryController@destroy')->name('categories.destroy');//destroy


//    put/patch -> update
//    delete -> delete
//    post -> store

        /*Product routes*/


    });

});

//Route::get('/home', function (){
//    return view('backend.home');
//});


Route::get('/cart', 'CartController@shoppingCart')->name('cart');
Route::get('/cart/{productId}/remove', 'CartController@removeFromCart')->name('carts.remove_product');
Route::post('/add-to-cart', 'CartController@addToCart');
Route::get('/{categoryId?}', 'PublicController@index');
Route::get('/category/{category}', 'PublicController@productsByCategoryId')->name('products_by_category');
Route::get('/products/{product}', 'PublicController@show')->name('product_details');


/**Check out Routes */
Route::get('/checkout/index', 'CheckoutController@index')->name('checkout.index');
Route::post('/checkout/store', 'CheckoutController@store')->name('checkout.store');


Route::get('/cart/index', 'CartController@index')->name('cart.index');
Route::post('/cart/store', 'CartController@store')->name('cart.store');
//Route::get('/cart/update', 'CheckoutController@index')->name('cart.index');
//Route::post('/cart/delete', 'CheckoutController@store')->name('cart.delete');


