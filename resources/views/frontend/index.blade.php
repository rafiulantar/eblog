@extends('frontend.layouts.master')


@section('content')

    <div class="row">

        <div class="col-lg-3">

            <h1 class="my-4">Categories</h1>
            <div class="list-group">
                @foreach($categories as $category)
                    <a href="{{ url('/'.$category->id) }}" class="list-group-item">{{ $category->title}}</a>
                @endforeach
            </div>

            <hr>

            {!! Form::open([
                'url'=>'/',
                'method'=>'get'
            ]) !!}

                {!! Form::text('keyword', null, [
                    'class'=>'form-control',
                    'placeholder'=>'Product Title'
                ]) !!}
                {!! Form::button('Filter', [
                    'class'=>'btn btn-info',
                    'type'=>'submit'
                ]) !!}

            {!! Form::close() !!}
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

            <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="row">

                @foreach($products as $product)
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="card h-100">

                            @if(file_exists(storage_path().'/app/public/products/'.$product->picture) && (!is_null($product->picture)))
                                <a href="#"><img class="card-img-top" src="{{ asset('storage/products/'.$product->picture) }}" alt=""></a>
                            @else
                                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                            @endif

                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="{{ route('product_details', [$product->id]) }}">{{ $product->title }}</a>
                                </h4>
                                <h5>{{ $product->price }} TK</h5>
                                <p class="card-text">{{ Str::limit($product->description, 150) }}</p>
                            </div>
                            <div class="card-footer">
                               {{--  @if(!in_array($product->id, $productsInCart))
                                 <button class="btn btn-sm btn-primary" id="{{ $product->id }}" onclick="addToCart({{ $product->id }})">Add To Cart</button>
                                @else
                                    <button class="btn btn-sm btn-info">Already In Cart</button>
                                @endif --}}


                               <form class="form-inline" action="{{ route('cart.store') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <button type="submit" class="btn btn-info" ><i class="fa fa-plus">Add to Cart</i> </button>


                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>


            {{ $products->links() }}
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>

@endsection

{{-- @push('scripts')
<script>

    function addToCart(id){
        // alert('adding '+id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            url: '/add-to-cart',
            data: {product_id: id},
            cache: false,
            success: function (res) {

                $('#'+id).removeClass('btn-primary');
                $('#'+id).addClass('btn-info');
                $('#'+id).text('Added To Cart');

            //     $('#addToCart'+id).html(`
            //     <span type="submit" class="hub-cart phub-cart btn">
            //         <i class="fas fa-shopping-bag" aria-hidden="true"></i> Already In Cart
            //     </span>
            // `);
            },
            error: function (xhr, status, error) {
                console.log("An AJAX error occured: " + status + "\nError: " + error);
            }
        });

    }

</script>
@endpush --}}
