<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('ui/frontend') }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('ui/frontend') }}/css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
@include('frontend.layouts.partials.navbar')

<!-- Page Content -->
<div class="container">

    @yield('content')
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
@include('frontend.layouts.partials.footer')

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('ui/frontend') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('ui/frontend') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
@stack('scripts')

</body>

</html>
