@extends('frontend.layouts.master')


@section('content')

<div class="row">

    <div class="col-lg-3">
        <h1 class="my-4">Shop Name</h1>
        <div class="list-group">
            @foreach($categories as $category)
                <a href="{{ url('/'.$category->id) }}" class="list-group-item">{{ $category->title}}</a>
            @endforeach
        </div>
    </div>
    <!-- /.col-lg-3 -->

    <div class="col-lg-9">

        <div class="card mt-4">
            @if(file_exists(storage_path().'/app/public/products/'.$product->picture) && (!is_null($product->picture)))
                <a href="#"><img class="card-img-top" src="{{ asset('storage/products/'.$product->picture) }}" alt=""></a>
            @else
                <img class="card-img-top img-fluid" src="http://placehold.it/900x400" alt="">
            @endif
            <div class="card-body">
                <h3 class="card-title">{{ $product->name }}</h3>
                <h4>{{ $product->price }} TK</h4>
                <p class="card-text">{{ $product->description }}</p>
                <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                4.0 stars
            </div>
        </div>
        <!-- /.card -->

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
                Product Reviews
            </div>
            <div class="card-body">

                @foreach($product->comments as $comment)
                <p>{{ $comment->body }}</p>
                <small class="text-muted">Posted by {{ $comment->commentedBy->name }} on {{ $comment->created_at->toFormattedDateString() }} <mark>{{ $comment->created_at->diffForHumans() }}</mark></small>
                <hr>
                @endforeach

                {!! Form::open([
                    'route'=>['products.review', $product->id]
                ]) !!}

                <div class="form-group">
                    {!! Form::textarea('body', null, [
                        'class'=>'form-control'
                    ]) !!}
                </div>

                {!! Form::button('Leave a Review', [
                    'class'=>'btn btn-success',
                    'type'=>'submit'
                ]) !!}

                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col-lg-9 -->

</div>

@endsection
