<div class="form-group">
    {!! Form::label('categoryId', 'Category') !!}

    {!! Form::select('category_id', $categories, null, [
        'class'=>'form-control',
        'id'=>'title',
        'placeholder'=>'Select one'
    ]) !!}
</div>
<div class="form-group">
    {{--                    <label for="title">Title</label>--}}
    {!! Form::label('title', 'Title') !!}
    {{--                    <input name="title" type="text" value="{{ old('title') }}" class="form-control" id="title">--}}
    {!! Form::text('title', null, [
        'class'=>'form-control',
        'id'=>'title',
    ]) !!}
</div>
<div class="form-group">
    <label for="description">Description</label><br>
    {{--                    <textarea name="description"  class="form-control" id="description">{{ old('description') }}</textarea>--}}
    {!! Form::textarea('description', null, [
       'class'=>'form-control',
       'id'=>'title',
   ]) !!}
</div>

<div class="form-group">
    <label for="picture">Image</label><br>
    {{--                    <textarea name="description"  class="form-control" id="description">{{ old('description') }}</textarea>--}}
    {!! Form::file('picture', null, [
       'class'=>'form-control',
       'id'=>'picture',
   ]) !!}
</div>

<p>Sizes : </p>
<div class="form-group form-check">
    @foreach($sizes as $key=>$size)
        <div>
{{--            <input name="size[]" type="checkbox" class="form-check-input" value="{{ $key }}" id="{{ $size }}">--}}
{{--            <label  class="form-check-label" for="{{ $size }}">{{ $size }}</label>--}}
            {!! Form::checkbox('size[]', $key, in_array($key, $selectedSizes), [
               'class'=>'form-check-input',
               'id'=>$size
            ]) !!}
            {{ $size }}
        </div>
    @endforeach
</div>

<div class="form-group">
    <label for="price">Price</label><br>
    {!! Form::number('price', null, [
       'class'=>'form-control',
       'id'=>'price',
   ]) !!}
</div>

<div class="form-group form-check">
    <input name="is_active" type="checkbox" class="form-check-input" id="isActive">
    <label  class="form-check-label" for="isActive">Is Active</label>
</div>
