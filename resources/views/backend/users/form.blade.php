<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

    <div class="col-md-6">

        {{ Form::text('name', null, [
            'class'=>'form-control',
            'required',
            'id'=>'name'
        ]) }}

        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

    <div class="col-md-6">
        {{ Form::email('email', null, [
                  'class'=>'form-control',
                  'required',
                  'id'=>'email'
              ]) }}
        @error('email')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="age" class="col-md-4 col-form-label text-md-right">Age</label>

    <div class="col-md-6">

        {{ Form::number('age', null, [
           'class'=>'form-control',
           'required',
           'id'=>'age'
       ]) }}

        @error('age')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="facebookUri" class="col-md-4 col-form-label text-md-right">Facebook URI</label>

    <div class="col-md-6">

        {{ Form::url('facebook_uri', $user->profile->facebook_uri??null, [
           'class'=>'form-control',
           'required',
           'id'=>'facebookUri'
       ]) }}

        @error('facebook_uri')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="bio" class="col-md-4 col-form-label text-md-right">Bio</label>
    <div class="col-md-6">

        {!! Form::textarea('bio', $user->profile->bio??null, [
            'class'=>'form-control',
            'id'=>'bio',
        ]) !!}

        @error('bio')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

    <div class="col-md-6">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

    <div class="col-md-6">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
    </div>
</div>

<p>Roles : </p>
<div class="form-group form-check">
    @foreach($roles as $key=>$role)
        <div>
            {!! Form::checkbox('roles[]', $key, in_array($key, $selectedRoles), [
               'class'=>'form-check-input',
               'id'=>$role
            ]) !!}
            {{ $role }}
        </div>
    @endforeach
</div>


<div class="form-group row">
    <label for="activeRole" class="col-md-4 col-form-label text-md-right">Active Role</label>

    <div class="col-md-6">
        {!! Form::select('active_role_id', $roles, null, [
            'class'=>'form-control',
            'id'=>'activeRole'
        ]) !!}
    </div>
</div>
