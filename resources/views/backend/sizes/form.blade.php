<div class="form-group">
    {{--                    <label for="title">Title</label>--}}
    {!! Form::label('title', 'Title') !!}
    {{--                    <input name="title" type="text" value="{{ old('title') }}" class="form-control" id="title">--}}
    {!! Form::text('title', null, [
        'class'=>'form-control',
        'id'=>'title',
    ]) !!}
</div>
