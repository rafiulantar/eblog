<div class="form-group row">


   {{ Form::label('name', 'Name') }}
    <div class="col-md-6">

        {{ Form::text('name', null, [
            'class'=>'form-control',
            'required',
            'id'=>'name'
        ]) }}

    </div>
</div>

<p>Permissions : </p>
<div class="form-group form-check">
    @foreach($permissions as $key=>$permission)
        <div>
            {!! Form::checkbox('permissions[]', $key, in_array($key, $selectedPermissions), [
               'class'=>'form-check-input',
               'id'=>$permission
            ]) !!}
            {{ $permission }}
        </div>
    @endforeach
</div>

