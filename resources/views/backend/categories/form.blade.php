<div class="form-group">
    {{--                    <label for="title">Title</label>--}}
    {!! Form::label('title', 'Title') !!}
    {{--                    <input name="title" type="text" value="{{ old('title') }}" class="form-control" id="title">--}}
    {!! Form::text('title', null, [
        'class'=>'form-control',
        'id'=>'title',
    ]) !!}
</div>
<div class="form-group">
    <label for="description">Description</label><br>
    {{--                    <textarea name="description"  class="form-control" id="description">{{ old('description') }}</textarea>--}}
    {!! Form::textarea('description', null, [
       'class'=>'form-control',
       'id'=>'title',
   ]) !!}
</div>
<div class="form-group form-check">
    <input name="is_active" type="checkbox" class="form-check-input" id="isActive">
    <label  class="form-check-label" for="isActive">Is Active</label>
</div>
